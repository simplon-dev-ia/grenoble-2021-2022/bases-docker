# Images Docker

Maintenant que nous savons commencer instantier des conteneurs à partir d'images
Docker pré-construites, il est temps d'apprendre à construire ces fameuses images !

## Rédiger un Dockerfile

La définition d'une image Docker passe par l'écriture d'un
[Dockerfile](https://docs.docker.com/engine/reference/builder/).

La syntaxe de ce fichier est déclarative et peut se résumer à un squelette
tel que :

```dockerfile
# Définir l'image de base
# Ex : python:3.9
FROM base-image:tag

# Copier les fichiers de l'application dans le conteneur
# Ex : fichiers sources Python, Jupyter, HTML, CSS, JS, etc.
COPY ./some/local/file /app/my/file

# Exécuter une commande dans le conteneur au moment de la construction
# Ex : installer des paquets système, installer des dépendances, etc.
RUN some-command -a some-arg1 -b some-arg2

# Définir la commande à exécuter par défaut lors du lancement du conteneur
CMD ["mycommand", "-a", "option1", "-b", "option2"]
```

Une application Flask basique est disponible dans le dépôt ([`app.py`](app.py)).
Nous allons tenter d'écrire un fichier `Dockerfile` pour conteneuriser cette application !

**Exercice**

- [ ] Vérifier que l'application Flask fonctionne dans l'environnement virtuel
- [ ] Créer un fichier `Dockerfile`
- [ ] Ecrire le Dockerfile avec les étapes suivantes :
    - Utiliser l'image de base `python:3.9-slim`
    - Ajouter les fichiers nécessaires à l'application (`app.py`, `Pipfile`, `Pipfile.lock`)
    - Installer le programme `pipenv` avec `pip`
    - Installer les dépendances de l'application avec `pipenv`
    - Définir la commande de lancement de l'application avec `gunicorn`

## Construction de l'image

Une fois le fichier `Dockerfile` écrit, nous pouvons désormais tenter de construire
l'image Docker de l'application !

En supposant le fichier `Dockerfile` dans le même dossier, voici la commande type
pour constuire une image Docker :

```bash
docker build -t monimage:montag .
```

Si des erreurs surviennent pendant la construction de l'image, il faudra modifier
le fichier `Dockerfile` en conséquence.

**Exercice**

- [ ] Construire l'image Docker de l'application nommée `flask-app`
- [ ] Instantier l'application sous forme de conteneur à partir de l'image Docker
- [ ] Accéder à l'application conteneurisée via le navigateur !
