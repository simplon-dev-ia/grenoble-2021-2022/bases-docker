# Installation

Pour pouvoir utiliser Docker, il est nécessaire de l'installer sur son système
local avant toute manipulation.

Documentation :

- https://docs.docker.com
- https://docs.docker.com/compose/


## Docker

Suivez les étapes officielles d'installation pour votre système : https://docs.docker.com/get-docker/

Sur les systèmes Linux, le plus simple est d'utiliser le script automatique :

```bash
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
```

Pour vérifier l'installation :

```bash
docker --version
```

## Docker Compose

Suivez les étapes officielles d'installation pour votre système : https://docs.docker.com/compose/install/#install-compose

Sur les systèmes Linux, il est possible de simplement télécharger l'exécutable :

```bash
DOCKER_CONFIG=${DOCKER_CONFIG:-/usr/local/lib/docker/cli-plugins}
mkdir -p $DOCKER_CONFIG/cli-plugins
curl -SL https://github.com/docker/compose/releases/download/v2.4.1/docker-compose-linux-x86_64 -o $DOCKER_CONFIG/cli-plugins/docker-compose
sudo chmod +x /usr/local/lib/docker/cli-plugins/docker-compose
```

Pour vérifier l'installation :

```bash
docker compose version
```
