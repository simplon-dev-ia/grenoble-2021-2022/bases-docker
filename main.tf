terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.3.0"
    }
  }

  backend "http" {
  }
}

locals {
  grenoble_ia1_p7 = {
    principal_id = "a0c131e3-e748-461f-89a4-b344a5ed68fd"
  }
}

provider "azurerm" {
  features {
    resource_group {
      prevent_deletion_if_contains_resources = false
    }
  }
}

data "azurerm_subscription" "subscription" {
}

resource "azurerm_resource_group" "default" {
  name     = "activity-docker"
  location = "France Central"
}

resource "azurerm_role_assignment" "rg_role_assignment" {
  scope                = azurerm_resource_group.default.id
  role_definition_name = "Contributor"
  principal_id         = local.grenoble_ia1_p7.principal_id
}

resource "azurerm_service_plan" "default" {
  name                = "activity-docker-asp"
  location            = azurerm_resource_group.default.location
  resource_group_name = azurerm_resource_group.default.name
  os_type             = "Linux"
  sku_name            = "F1"
}

resource "azurerm_container_registry" "default" {
  name                = "activityDockerAcr"
  resource_group_name = azurerm_resource_group.default.name
  location            = azurerm_resource_group.default.location
  sku                 = "Basic"
  admin_enabled       = true
}
