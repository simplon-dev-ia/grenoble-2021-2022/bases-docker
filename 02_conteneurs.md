# Conteneurs Docker

La façon la plus simple pour commencer à utiliser Docker est d'utiliser des
images déjà créées pour instantier des conteneurs ! Les images Docker publiques
sont généralement hébergées sur la plateforme [Docker Hub](https://hub.docker.com).

Par exemple, l'application [Metabase](https://www.metabase.com) est disponible
sous la forme d'une [image Docker](https://www.metabase.com/docs/latest/operations-guide/running-metabase-on-docker.html).

Documentation :

- https://docs.microsoft.com/en-us/azure/container-registry/
- https://docs.microsoft.com/en-us/azure/app-service/

## Lancer un conteneur en local

Pour manipuler des images et conteneurs Docker, toutes les commandes passent
par l'exécutable `docker`.

Quelques exemples de commandes (en fonction du système, il est peut être nécessaire
de précéder les commandes par `sudo`) :

- Instantier un conteneur à partir d'une image Docker accessible via le port 3000 :
```bash
docker run -d -p "3000:5000" <image:tag>
```

- Lister les conteneurs instantiés :
```bash
docker ps -a
```

- Afficher les logs d'un conteneur :
```bash
docker logs -f <container_id>
```

- Ouvrir un shell dans un conteneur :
```bash
docker exec -it metabase sh
```

- Arrêter un conteneur :
```bash
docker stop <container_id>
```

- Supprimer un conteneur :
```bash
docker rm <container_id>
```

**Exercice**

- [ ] Trouver le nom de l'image Docker de l'application Metabase sur Docker Hub
- [ ] Instantier un conteneur à partir de la dernière version de l'image Docker de Metabase
- [ ] Faire en sorte que l'application soit accessible en local sur le port 8000
- [ ] Vérifier que l'application Metabase fonctionne à l'URL http://localhost:8000
- [ ] Arrêter et détruire le conteneur de l'application Metabase

## Lancer un conteneur sur Azure

Il est possible de lancer un conteneur dans le cloud Azure ! Pour cela, deux
ressources sont nécessaires :

- Un registre de conteneurs (_Azure Container Registry_)
- Une application web avec Docker (_Azure App Service_)

Pour distribuer une application conteneurisée avec Docker sur un registre distant
(Docker Hub, Azure, GCP, etc.), la procédure est la suivante :

1. Se connecter au registre distant :
```bash
docker login urldemonregistre.io
```

2. Tagger en local l'image Docker avec comme préfixe l'URL du registre distant, par exemple :
```bash
docker tag monimage:montag urldemonregistre.io/monimage:montag
```

3. Pousser l'image taggée sur le registre distant :
```bash
docker push urldemonregistre.io/monimage:montag
```

Azure permet ensuite de déployer une application web directement à partir d'une
image Docker depuis un registre distant (Azure CR, Docker Hub, Registre privé).

**Exercice**

- [ ] Se connecter au registre de conteneur Azure créé pour cette activité
- [ ] Tagger l'image `metabase:latest` avec l'URL du conteneur distant
- [ ] Pousser l'image taggée pour le conteneur distant
- [ ] Créer une application web Azure avec l'image déployée (identifiant de l'app : `activite-docker-app-<prenom>`)
- [ ] Vérifier que Metabase est accessible à l'URL de l'application Azure

## Lancer plusieurs conteneurs en local

Pour pouvoir lancer plusieurs conteneurs à la fois et définir des configurations
plus complexes (ports, volumes, variables d'environnement, etc.), nous pouvons
utiliser [Docker Compose](https://docs.docker.com/compose/).

Le principe est simple : définir un fichier `docker-compose.yml` listant tous les
conteneurs (_services_) à exécuter avec leurs configurations respectives, et
utiliser des commandes agissant sur l'intégralité du fichier !

Le format du fichier `docker-compose.yml` :

```yaml
services:
  service_1:
    image: mon/image:version
    environment:
      MA_VARIABLE: "valeur"
    volumes:
      - "./mon/dossier:/dossier/dans/le/conteneur"
    ports:
      - "port-hote:port-conteneur"

  service_2:
    ...

  service_3:
    ...
```

Les commandes principales (en fonction du système, il est peut être nécessaire
de précéder les commandes par `sudo`):

- Lancer tous les conteneurs :
```bash
docker-compose up -d
```

- Observer l'état de tous les conteneurs :
```bash
docker-compose ps
```

- Observer les logs de tous les conteneurs :
```bash
docker-compose logs -f
```

- Arrêter et détruire tous les conteneurs :
```bash
docker-compose down
```

**Exercice**

- [ ] Créer un fichier `docker-compose.yml`
- [ ] Ajouter un service `postgres` avec la configuration suivante :
    - Version 14 de la base de données PostgreSQL
    - Spécifier le nom d'utilisateur, mot de passe et nom de la base
    - Spécifier un dossier hôte comme volume pour sauvegarder les données de la base
- [ ] Ajouter un service `metabase` avec la configuration suivante :
    - Dernière version de l'application Metabase
    - Utilisation de la base données Postgres comme stockage des données Metabase (lire la documentation !)
    - Accessible sur le port 8000
- [ ] Lancer la configuration Docker Compose
- [ ] Observer l'état et les logs des conteneurs
- [ ] Accéder à l'application Metabase à l'URL http://localhost:8000
- [ ] Arrêter et détruire tous les conteneurs
